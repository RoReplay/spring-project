package com.example.demo.services;

import com.example.demo.models.Book;
import com.example.demo.models.filters.BookFilter;

import java.util.List;

public interface BookService {
    List<Book> getAll();
    List<Book> getBookAll(String sort);
    List<Book> bookFilter(BookFilter bookFilter);
    Book view(Integer id);
    Integer countRecords();

    void update(Book book);
    void delete(Integer id);
    boolean create(Book book);
    List<String> getColumns();
    boolean creates(List<Book> books);
}
