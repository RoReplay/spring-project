package com.example.demo.services.impl;

import com.example.demo.models.Book;
import com.example.demo.models.filters.BookFilter;
import com.example.demo.repositories.BookRepository;
import com.example.demo.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    private BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }
    @Override
    public List<Book> getAll() {
        return this.bookRepository.getBookList();
    }
    @Override
    public List<Book> getBookAll(String sort) {
        return this.bookRepository.getBookListAll(sort);
    }

    @Override
    public List<Book> bookFilter(BookFilter bookFilter) {
        return this.bookRepository.bookFilter(bookFilter);
    }


    @Override
    public Book view(Integer id) {
       return this.bookRepository.getBook(id);
    }

    @Override
    public Integer countRecords() {
        return this.bookRepository.count();
    }

    @Override
    public void update(Book book) {
        this.bookRepository.update(book);
    }
    @Override
    public void delete(Integer id) {
        this.bookRepository.delete(id);
    }
    @Override
    public boolean create(Book book) {
        return this.bookRepository.create(book);
    }

    @Override
    public List<String> getColumns() {
        return this.bookRepository.getColumns();
    }

    @Override
    public boolean creates(List<Book> books) {
        return this.bookRepository.creates(books);
    }
}
