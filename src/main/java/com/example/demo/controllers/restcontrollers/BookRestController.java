package com.example.demo.controllers.restcontrollers;

import com.example.demo.models.Book;
import com.example.demo.models.filters.BookFilter;
import com.example.demo.services.BookService;
import com.example.demo.services.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/book")
public class BookRestController {
    private BookService bookService;
    private UploadService uploadService;
    @Autowired
    public BookRestController(BookService bookService, UploadService uploadService) {
        this.bookService = bookService;
        this.uploadService = uploadService;
    }

    @GetMapping("/all")
    public List<Book> getAll() {
        List<Book> books = this.bookService.getAll();
        return books;
    }
    @GetMapping("/filter")
    public List<Book> getFilter(BookFilter bookFilter) {
        List<Book> books = this.bookService.bookFilter(bookFilter);
        return books;
    }
    @GetMapping("/{id}")
    public Map<String, Object> findOne(@PathVariable("id") Integer id) {
       Map<String, Object> response = new HashMap<>();
       Book book = this.bookService.view(id);
       if (book != null) {
           response.put("Status", "Founded");
           response.put("Book", book);
       } else {
           response.put("Status", "Not Founded");
       }
       return response;
    }
    @PostMapping("")
    public Map<String, Object> createBook(@RequestBody Book book) {
        Map<String, Object> response = new HashMap<>();
        response.put("Status", "Successfully Created");
        response.put("Book", this.bookService.create(book));
        return response;
    }
    @PutMapping("")
    public Map<String, Object> update(@RequestBody Book book) {
        Map<String, Object> response = new HashMap<>();
         this.bookService.update(book);
         if (book != null) {
             response.put("Status", true);
             response.put("Result", "Successfully Updated");
         } else {
             response.put("Status", "False");
             response.put("Result", "Book Not Found");
         }
         return response;
    }
    @PostMapping("/upload")
    public Map<String, Object> upload(@RequestParam("file_btb") MultipartFile file) {
        String filename = this.uploadService.upload(file);

        Map<String, Object> response = new HashMap<>();
        if (filename != null) {
            response.put("Status", true);
            response.put("File", filename);
        } else {
            response.put("Status", false);
            response.put("File", "Not Found");

        }

        return response;
    }
    @DeleteMapping("/{id}")
    public Map<String, Object> deleteBook(@PathVariable("id") Integer id) {
        Map<String, Object> response = new HashMap<>();
        this.bookService.delete(id);
        response.put("status", true);
        response.put("message", "Delete OK looking good");
        return response;
    }
    @PostMapping("/books")
    public Map<String, Object> creates(@RequestBody List<Book> books) {
        Map<String, Object> response = new HashMap<>();
        this.bookService.creates(books);
        response.put("status", true);
        response.put("books", books);
        return response;
    }

}
