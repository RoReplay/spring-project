package com.example.demo.controllers.restcontrollers;

import com.example.demo.models.Category;
import com.example.demo.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/category")
public class CategoryRestController {

    private CategoryService categoryService;

    @Autowired
    public CategoryRestController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @GetMapping("/all")
    public Map<String, Object> getAll() {
        Map<String, Object> response = new HashMap<>();
        List<Category> categoryList = this.categoryService.getCategories();
        if (categoryList != null) {
            response.put("Status", true);
            response.put("Category", categoryList);
            response.put("Size", categoryList.size());
        } else {
            response.put("Status", false);
            response.put("Categories", "Not Found");

        }
        return response;
    }
}
