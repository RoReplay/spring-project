package com.example.demo.controllers;

import com.example.demo.models.Book;
import com.example.demo.models.Category;
import com.example.demo.models.filters.BookFilter;
import com.example.demo.services.BookService;
import com.example.demo.services.CategoryService;
import com.example.demo.services.UploadService;
import org.hibernate.validator.constraints.ParameterScriptAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class BookController {
    private BookService bookService;
    private CategoryService categoryService;
    private UploadService uploadService;

    @Autowired
    public void setUploadService(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    @Autowired
    public BookController(BookService bookService, CategoryService categoryService) {
        this.bookService = bookService;
        this.categoryService = categoryService;
    }

    @GetMapping({"/index", "/"})
    public String index(String sort, Model model, BookFilter bookFilter) {
//        List<Book> bookList = this.bookService.getAll();
        System.out.println(bookFilter.getSortBy());
        List<Book> books = null;
//        if (bookFilter.getCate_id() != null && bookFilter.getTitle() != null && bookFilter.getSortBy() != null) {
            books = this.bookService.bookFilter(bookFilter);
//        }
/*        else {
            books = this.bookService.getBookAll(sort);
            System.out.println("b");
        }*/
        List<Category> categories = this.categoryService.getCategories();
        model.addAttribute("categories",categories);
        model.addAttribute("books", books);//books for html to catch, attribute bookList is list of booklist
        model.addAttribute("columns", this.bookService.getColumns());
        model.addAttribute("records", this.bookService.countRecords());
        return "book/index";
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable("id") Integer id, Model model) {
        Book book = this.bookService.view(id);
        model.addAttribute("book", book);
        return "book/view-detail";
    }
    @GetMapping("/update/{book_id}")
    public String update(@PathVariable("book_id") Integer id, Model model) {
        Book book = this.bookService.view(id);
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("isNew", false);
        model.addAttribute("book", book);
        return "book/create";
    }

    @PostMapping("/update/submit")
    public String updateSubmit(@ModelAttribute("book") Book book, MultipartFile file) { //Modelattribute is used to select attribute got from submit

        String fileName= this.uploadService.singleFileUpload(file, null);
        if (!file.isEmpty()) {
            book.setThumbnail("/images-btb/" + fileName);
        }
        System.out.println(book.getThumbnail());
        bookService.update(book);
        return "redirect:/index";
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id) {
        System.out.println(id);
        bookService.delete(id);
        return "redirect:/index";
    }
    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("isNew", true);
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("book", new Book());
        return "/book/create";
    }

    @PostMapping("/create/submit")
    public String createSubmit(@Valid Book book, BindingResult bindingResult, MultipartFile file, Model model) {
        System.out.println(book);
        if (bindingResult.hasErrors()) {
            model.addAttribute("isNew", true);
            model.addAttribute("categories", categoryService.getCategories());
            return "book/create";
        }
        System.out.println(book);
        String fileName = this.uploadService.singleFileUpload(file, "myfile/");


        book.setThumbnail("/images-btb/" + fileName);
        this.bookService.create(book);
        return "redirect:/index";
    }
    @GetMapping("/multiupload")
    public String showUpload() {
        return "book/upload-file";
    }
    @PostMapping("/multiupload/submit")
    public String submitMultiple(@RequestParam("file") List<MultipartFile> files) {
        List<String> filenames = this.uploadService.upload(files, "myfiles");
        filenames.forEach(filea -> System.out.println(filea));
        return "redirect:/index";
    }

    @GetMapping("/create-cat")
    public String createCat(Model model) {

        model.addAttribute("category", new Category());

        return "category/create-cat";
    }
    @PostMapping("/create-cat/submit")
    public String createCatSubmit(@ModelAttribute("Category") Category category){
        this.categoryService.createCat(category);

        return "redirect:/index";
    }
}
