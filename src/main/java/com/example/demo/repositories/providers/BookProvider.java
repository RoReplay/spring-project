package com.example.demo.repositories.providers;

import com.example.demo.models.Book;
import com.example.demo.models.filters.BookFilter;
import com.sun.org.glassfish.gmbal.ParameterNames;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

public class BookProvider {
    public String getAllProvider() {
        return new SQL(){{
            SELECT("*");
            FROM("tb_book b");
            INNER_JOIN("tb_category c ON c.cate_id = b.cid");
            ORDER_BY("b.id");
        }}.toString();
    }

    public String bookFilterProvider(BookFilter bookFilter) {
        System.out.println(bookFilter);
        return new SQL(){{
            SELECT("*");
            FROM("tb_book b");
            INNER_JOIN("tb_category c ON c.cate_id = b.cid");
            if (bookFilter.getCate_id() != null) {
                WHERE("c.cate_id = #{cate_id}");
            }
            if (bookFilter.getTitle() != null) {
                WHERE("b.title iLIKE '%' || #{title} || '%'");
            }
//            ORDER_BY("b.title");
            if (bookFilter.getSortBy() != null) {
                if (bookFilter.getSortBy().equals("title")) {
                    System.out.println("True");
                    ORDER_BY("b.title");
                } else if (bookFilter.getSortBy().equals("author")) {
                    System.out.println("True");
                    ORDER_BY("b.author");
                } else if (bookFilter.getSortBy().equals("publisher")) {
                    System.out.println("True");
                    ORDER_BY("b.publisher");
                } else if (bookFilter.getSortBy().equals("thumbnail")) {
                    System.out.println("True");
                    ORDER_BY("b.thumbnail");
                } else if (bookFilter.getSortBy().equals("cid")) {
                    System.out.println("True123");
                    ORDER_BY("c.cname");
                } else {
                    ORDER_BY("b.id");
                }
            }
        }}.toString();
    }

    public String getAllProviderWithSort(String sort) {
        return new SQL(){{
            SELECT("*");
            FROM("tb_book b");
            INNER_JOIN("tb_category c ON c.cate_id = b.cid");
//            if (!sort.equals("cid")) {
            if (sort != null) {
                if (sort.equals("cid")) {
                    ORDER_BY("c.cname");
                }
                else {
                    ORDER_BY("b." + sort);
                }
            }
//            } else if (sort.equals("cid")) {
//            }
        }}.toString();
    }
    public String count() {
        return new SQL(){{
            SELECT("COUNT(id)");
            FROM("tb_book b");
            INNER_JOIN("tb_category c ON b.cid = c.cate_id");
        }}.toString();
    }


    public String insertProvider(Book book) {
        return new SQL() {{
            INSERT_INTO("tb_book");
            VALUES("title", "#{title}");
            VALUES("author", "#{author}");
            VALUES("publisher", "#{publisher}");
            VALUES("thumbnail", "#{thumbnail}");
            VALUES("cid", "#{category.c_id}");
        }}.toString();
    }

    public String deleteProvider(Integer id) {
        return new SQL(){{
            DELETE_FROM("tb_book");
            WHERE("id=#{id}");
        }}.toString();
    }
    public String updateProvider(Book book) {
        return new SQL(){{
            UPDATE("tb_book");
            SET("title=#{title}");
            SET("author=#{author}");
            SET("publisher=#{publisher}");
            SET("thumbnail=#{thumbnail}");
            SET("cid=#{category.c_id}");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String columnsProvider(){
        return new SQL(){{
            SELECT("column_name");
            FROM("information_schema.columns");
            WHERE("table_name = 'tb_book'");
        }}.toString();
    }
}
