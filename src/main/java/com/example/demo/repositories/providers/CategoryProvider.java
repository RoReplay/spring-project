package com.example.demo.repositories.providers;

import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
    public String getBookByCateIdProvider(Integer id) {
        return new SQL(){{
            SELECT("*");
            FROM("tb_book b");
            WHERE("b.cid = #{id}");
        }}.toString();
    }
}
