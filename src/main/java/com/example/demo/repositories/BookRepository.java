package com.example.demo.repositories;

import com.example.demo.models.Book;
import com.example.demo.models.filters.BookFilter;
import com.example.demo.repositories.providers.BookProvider;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.cache.StandardParsedTemplateEntryValidator;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface BookRepository {
    @SelectProvider(type = BookProvider.class,method = "getAllProvider") //Type is the name of the class, method is the name of the method
    @Results({
            @Result(column = "cate_id", property = "category.c_id"),
            @Result(column = "cname", property = "category.cname")
    })
    List<Book> getBookList();
    @SelectProvider(type = BookProvider.class,method = "getAllProviderWithSort")
    @Results({
            @Result(column = "cate_id", property = "category.c_id"),
            @Result(column = "cname", property = "category.cname")
    })
    List<Book> getBookListAll(String sort);

    @Select("select * from tb_book b INNER JOIN tb_category c ON b.cid=c.cate_id where id =#{id}")
     @Results({
            @Result(column = "cate_id", property = "category.c_id"),
            @Result(column = "cname", property = "category.cname")
    })
    Book getBook(@Param("id") Integer id);

    @SelectProvider(type = BookProvider.class,method = "bookFilterProvider")
    @Results({
            @Result(column = "cate_id", property = "category.c_id"),
            @Result(column = "cname", property = "category.cname")
    })
    List<Book> bookFilter(BookFilter bookFilter);


    @SelectProvider(type = BookProvider.class, method = "count")
    Integer count();

    @UpdateProvider(type = BookProvider.class, method="updateProvider")
    void update(Book book);


    @DeleteProvider(type = BookProvider.class, method = "deleteProvider")
    void delete(Integer id);

    @InsertProvider(type = BookProvider.class, method = "insertProvider")
    @SelectKey(
            keyProperty = "id",
            keyColumn = "curr_id",
            statementType = StatementType.PREPARED,
            before = false,
            resultType = Integer.class,
            statement = "SELECT currval('tb_book_id_seq') AS curr_id"
    )
     boolean create(Book book);

//    @Select("SELECT column_name FROM information_schema.columns WHERE table_name = 'tb_book' LIMTI 5")
    @SelectProvider(type = BookProvider.class, method = "columnsProvider")
    List<String> getColumns();

    @Insert({
            "<script>" +
                    "INSERT INTO tb_book(title, author, publisher, thumbnail, cid)" +
                    "VALUES <foreach collection='books' item='book' index='ind' separator=','>(" +
                    "#{book.title}," +
                    "#{book.author}," +
                    "#{book.publisher}," +
                    "#{book.thumbnail}," +
                    "#{book.category.c_id}" +
                    ")</foreach>" +
                    "</script>"
    })
    @SelectKey(
            keyProperty = "id",
            keyColumn = "curr_id",
            statementType = StatementType.PREPARED,
            before = false,
            resultType = Integer.class,
            statement = "SELECT currval('tb_book_id_seq') AS curr_id"
    ) //????????????????
   boolean creates(@Param("books") List<Book> books);





  /*  List<Book> bookList = new ArrayList<>();
    Faker faker = new Faker();
    {
        for (int i = 1; i < 11; i++) {
            Book book = new Book();
            book.setId(i);
            book.setTitle(faker.book().title());
            book.setAuthor(faker.book().author());
            book.setPublisher(faker.book().publisher());
            bookList.add(book);
        }
    }

    public List<Book> getBookList() {
        return this.bookList;
    }

    public Book getBook(Integer id) {
        for (Book book : bookList) {
            if (book.getId() == id) {
                return book;
            }
        }
        return null;
    }
    public void update(Book book) {
        for (int i = 0; i < bookList.size(); i++) {
            if(bookList.get(i).getId() == book.getId()) {
                bookList.set(i, book);
                System.out.println("Success");
                return;
            }
        }
        System.out.println("Failed");
    }
    public void delete(Integer id) {
        System.out.println(id);
        for (int i = 0; i < bookList.size(); i++) {
            if (bookList.get(i).getId() == id) {
                bookList.remove(i);
                System.out.println("Deleted");
                System.out.println(bookList);
                return;
            }
        }
        System.out.println("Failed To Remove");
    }
    public boolean create(Book book) {
        return bookList.add(book);
    }*/
}
