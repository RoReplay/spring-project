package com.example.demo.repositories;

import com.example.demo.models.Book;
import com.example.demo.models.Category;
import com.example.demo.repositories.providers.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    @Select("SELECT * FROM tb_category")
    @Results({
            @Result(column = "cate_id", property = "c_id"),
            @Result(property = "books", column = "cate_id", many = @Many(select = "getBookByCateId"))
    })
    List<Category> categories();

    @SelectProvider(method = "getBookByCateIdProvider", type = CategoryProvider.class)
    List<Book> getBookByCateId(@Param("c_id") Integer id);

    @Insert("INSERT INTO tb_category(cname, description) VALUES(#{cname}, #{description})")
    Boolean createCat(Category category);
}
