package com.example.demo.models.filters;

public class BookFilter {
    private Integer cate_id;
    private String title;
    private String sortBy;

    public BookFilter() {
    }

    public BookFilter(Integer cate_id, String title, String sortBy) {
        this.cate_id = cate_id;
        this.title = title;
        this.sortBy = sortBy;
    }

    public Integer getCate_id() {
        return cate_id;
    }

    public void setCate_id(Integer cate_id) {
        this.cate_id = cate_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    @Override
    public String toString() {
        return "BookFilter{" +
                "cate_id=" + cate_id +
                ", title='" + title + '\'' +
                ", sortBy='" + sortBy + '\'' +
                '}';
    }
}
