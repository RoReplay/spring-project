package com.example.demo.models;

import java.util.List;

public class Category {
    private Integer c_id;
    private String cname;
    private List<Book> books;
    private String description;
    public Category() {
    }

    public Category(Integer c_id, String cname, List<Book> books, String description) {
        this.c_id = c_id;
        this.cname = cname;
        this.books = books;
        this.description = description;
    }

    public Integer getC_id() {
        return c_id;
    }

    public void setC_id(Integer c_id) {
        this.c_id = c_id;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Category{" +
                "c_id=" + c_id +
                ", cname='" + cname + '\'' +
                ", books=" + books +
                ", description='" + description + '\'' +
                '}';
    }
}
