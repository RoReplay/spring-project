CREATE TABLE tb_category (
  cate_id SERIAL PRIMARY KEY,
  cname VARCHAR,
  description VARCHAR
);

CREATE TABLE tb_book (
  id SERIAL PRIMARY KEY,
  title VARCHAR,
  author VARCHAR,
  publisher VARCHAR,
  thumbnail VARCHAR,
  cid INT,
  CONSTRAINT fk_cat FOREIGN KEY(cid) REFERENCES tb_category
);

